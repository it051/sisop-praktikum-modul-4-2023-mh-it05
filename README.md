# LAPRES MODUL 4 IT05

## SOAL 1

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>   


#define GALLERY_PATH "/home/p/modul4/soal1/data/gallery/"
#define REV_FOLDER "rev"
#define DELETE_FOLDER "delete"


void processRevFolder(const char *folderPath);
void processDeleteFolder(const char *folderPath);
void reverseFileName(char *fileName);
void createFolderIfNotExists(const char *folderPath);


int main() {
     
    createFolderIfNotExists(GALLERY_PATH REV_FOLDER);
    createFolderIfNotExists(GALLERY_PATH DELETE_FOLDER);


    while (1) {
        processRevFolder(GALLERY_PATH REV_FOLDER);
        processDeleteFolder(GALLERY_PATH DELETE_FOLDER);
        sleep(1);  
    }


    return 0;
}


void createFolderIfNotExists(const char *folderPath) {
    struct stat st = {0};


    if (stat(folderPath, &st) == -1) {
         
        if (mkdir(folderPath, 0777) == -1) {
            perror("Unable to create folder");
            exit(EXIT_FAILURE);
        }
    }
}


void processRevFolder(const char *folderPath) {
    DIR *dir;
    struct dirent *entry;


    dir = opendir(folderPath);


    if (dir == NULL) {
        perror("Unable to open directory");
        exit(EXIT_FAILURE);
    }


    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) {  
            char oldPath[512];   
            char newPath[512];  


            snprintf(oldPath, sizeof(oldPath), "%s/%s", folderPath, entry->d_name);
            snprintf(newPath, sizeof(newPath), "%s/%s", GALLERY_PATH, entry->d_name);


            
            reverseFileName(newPath);


            
            rename(oldPath, newPath);
        }
    }


    closedir(dir);
}


void processDeleteFolder(const char *folderPath) {
    DIR *dir;
    struct dirent *entry;


    dir = opendir(folderPath);


    if (dir == NULL) {
        perror("Unable to open directory");
        exit(EXIT_FAILURE);
    }


    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) { 
            char filePath[512];  
            snprintf(filePath, sizeof(filePath), "%s/%s", folderPath, entry->d_name);


            
            remove(filePath);
        }
    }


    closedir(dir);
}


void reverseFileName(char *fileName) {
    int i, j;
    char temp;


    for (i = 0, j = strlen(fileName) - 1; i < j; i++, j--) {
        temp = fileName[i];
        fileName[i] = fileName[j];
        fileName[j] = temp;
    }
}



void processRevFolder(const char *folderPath);
void processDeleteFolder(const char *folderPath);
void reverseFileName(char *fileName);
void createFolderIfNotExists(const char *folderPath);
```
```void processRevFolder(const char *folderPath)```
- Fungsi ini menerima alamat folder (folderPath) dan kemungkinan melakukan pemrosesan terhadap file-file di dalamnya.
- Nama fungsi (processRevFolder) mungkin menunjukkan bahwa ini terkait dengan pemrosesan folder, tetapi tanpa melihat implementasinya, sulit untuk memberikan rincian lebih lanjut.

```void processDeleteFolder(const char *folderPath)```
- Fungsi ini menerima alamat folder (folderPath) dan kemungkinan melakukan penghapusan folder beserta isinya.
- Nama fungsi (processDeleteFolder) menunjukkan bahwa ini mungkin terkait dengan menghapus folder, tetapi implementasi sebenarnya harus dilihat untuk memahami dengan pasti.

```void reverseFileName(char *fileName)```
- Fungsi ini menerima nama file (fileName) dan mungkin membalik karakter-karakternya.
- Nama fungsi (reverseFileName) menunjukkan bahwa tujuannya adalah membalik nama file.

```void createFolderIfNotExists(const char *folderPath)```
- Fungsi ini menerima alamat folder (folderPath) dan mungkin bertujuan untuk membuat folder jika belum ada.
- Nama fungsi (createFolderIfNotExists) menunjukkan bahwa ini mungkin digunakan untuk memastikan bahwa folder yang diinginkan ada, dan jika belum ada, maka akan dibuat.

```c
void createFolderIfNotExists(const char *folderPath) {
    struct stat st = {0};


    if (stat(folderPath, &st) == -1) {
         
        if (mkdir(folderPath, 0777) == -1) {
            perror("Unable to create folder");
            exit(EXIT_FAILURE);
        }
    }
}
```

```struct stat st = {0};```
- Mendeklarasikan sebuah struktur stat yang disebut st. Struktur ini akan digunakan untuk menyimpan informasi status file atau folder.

```if (stat(folderPath, &st) == -1)```

- Menggunakan fungsi stat untuk mendapatkan informasi status file atau folder yang diberikan oleh folderPath.
- Jika pemanggilan stat mengembalikan nilai -1, ini menunjukkan bahwa folder tidak ditemukan.

```if (mkdir(folderPath, 0777) == -1):```
- Jika folder tidak ditemukan (berdasarkan hasil stat sebelumnya), maka blok ini akan dijalankan.
- Menggunakan fungsi mkdir untuk membuat folder dengan nama yang diberikan oleh folderPath.
- Angka 0777 adalah mode atau hak akses untuk folder yang baru dibuat.
- Jika pemanggilan mkdir mengembalikan nilai -1, itu berarti ada masalah dalam pembuatan folder.

```perror("Unable to create folder");```
- Jika terjadi kesalahan saat membuat folder, perror akan mencetak pesan kesalahan terkait ke stderr.

```exit(EXIT_FAILURE);```
- Jika terjadi kesalahan dalam membuat folder, program akan keluar dengan status kegagalan (EXIT_FAILURE).

```c
void processRevFolder(const char *folderPath) {
    DIR *dir;
    struct dirent *entry;


    dir = opendir(folderPath);


    if (dir == NULL) {
        perror("Unable to open directory");
        exit(EXIT_FAILURE);
    }


    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) {  
            char oldPath[512];   
            char newPath[512];  


            snprintf(oldPath, sizeof(oldPath), "%s/%s", folderPath, entry->d_name);
            snprintf(newPath, sizeof(newPath), "%s/%s", GALLERY_PATH, entry->d_name);


            
            reverseFileName(newPath);


            
            rename(oldPath, newPath);
        }
    }


    closedir(dir);
}
```
```DIR *dir; dan struct dirent *entry;```
- Mendeklarasikan pointer ke struktur DIR dan dirent. Struktur ini menyediakan informasi tentang file atau folder di dalam suatu direktori.

```dir = opendir(folderPath);```
- Membuka direktori yang diberikan oleh folderPath menggunakan fungsi opendir.
Jika direktori tidak dapat dibuka, mencetak pesan kesalahan dan keluar dari program dengan status kegagalan.

```while ((entry = readdir(dir)) != NULL)```
- Melakukan loop selama ada file atau folder yang dapat dibaca di dalam direktori.
- Setiap entri (file atau folder) diakses melalui variabel entry.

```if (entry->d_type == DT_REG)```
- Memeriksa apakah entri yang sedang diproses adalah file regular (bukan folder atau tipe lainnya).

```char oldPath[512];``` dan char ```newPath[512];```
- Mendeklarasikan array karakter untuk menyimpan path lama (oldPath) dan path baru (newPath) dari file yang sedang diproses.
- snprintf(oldPath, sizeof(oldPath), "%s/%s", folderPath, entry->d_name); dan snprintf(newPath, sizeof(newPath), "%s/%s", GALLERY_PATH, entry->d_name);:
- Menggunakan snprintf untuk membuat path lengkap untuk file yang sedang diproses, termasuk direktori utama (folderPath atau GALLERY_PATH) dan nama file (entry->d_name).

```reverseFileName(newPath);```
- Memanggil fungsi reverseFileName untuk membalikkan nama file pada newPath.

```rename(oldPath, newPath);```
- Menggunakan fungsi rename untuk mengganti nama file dari oldPath menjadi newPath. Ini pada dasarnya memindahkan file ke lokasi baru dengan nama yang telah dibalik.

```closedir(dir);```
- Menutup direktori setelah selesai memproses semua file.

```c
void processDeleteFolder(const char *folderPath) {
    DIR *dir;
    struct dirent *entry;


    dir = opendir(folderPath);


    if (dir == NULL) {
        perror("Unable to open directory");
        exit(EXIT_FAILURE);
    }


    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) { 
            char filePath[512];  
            snprintf(filePath, sizeof(filePath), "%s/%s", folderPath, entry->d_name);


            
            remove(filePath);
        }
    }


    closedir(dir);
}
```
```DIR *dir; dan struct dirent *entry;```
- Mendeklarasikan pointer ke struktur DIR dan dirent. Struktur ini menyediakan informasi tentang file atau folder di dalam suatu direktori.

```dir = opendir(folderPath);```
- Membuka direktori yang diberikan oleh folderPath menggunakan fungsi opendir.
- Jika direktori tidak dapat dibuka, mencetak pesan kesalahan dan keluar dari program dengan status kegagalan.

```while ((entry = readdir(dir)) != NULL)```
- Melakukan loop selama ada file atau folder yang dapat dibaca di dalam direktori.
- Setiap entri (file atau folder) diakses melalui variabel entry.

```if (entry->d_type == DT_REG)```
- Memeriksa apakah entri yang sedang diproses adalah file regular (bukan folder atau tipe lainnya).

```char filePath[512];```
- Mendeklarasikan array karakter untuk menyimpan path lengkap dari file yang sedang diproses.

```snprintf(filePath, sizeof(filePath), "%s/%s", folderPath, entry->d_name);```
- Menggunakan snprintf untuk membuat path lengkap untuk file yang sedang diproses, termasuk direktori utama (folderPath) dan nama file (entry->d_name).

```remove(filePath);```
- Menggunakan fungsi remove untuk menghapus file yang terletak di path yang diberikan (filePath).

```closedir(dir);```
- Menutup direktori setelah selesai memproses semua file.

**Algoritma rev**
```c
void reverseFileName(char *fileName) {
    int i, j;
    char temp;


    for (i = 0, j = strlen(fileName) - 1; i < j; i++, j--) {
        temp = fileName[i];
        fileName[i] = fileName[j];
        fileName[j] = temp;
    }
}
```

### output
![test](img/soal1img/gambar1.png)
![test](img/soal1img/gambar2.png)
![test](img/soal1img/gambar3.png)
![test](img/soal1img/gambar4.png)


## SOAL 2

### open-password.c
Bagian import library dan define panjang password
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <openssl/buffer.h>

#define MAX_PASSWORD_LENGTH 256
```

Bagian fungsi untuk decode 
```c
char *base64_decode(const char *encoded){
    //deklarasi variabel
    BIO *bio, *b64;
    int encoded_size = strlen(encoded);

    //alokasi memori buffer
    char *buffer = (char *)malloc(encoded_size);
    memset(buffer, 0, encoded_size);

    //inisialisasi struktur BIO
    bio = BIO_new_mem_buf(encoded, -1);
    b64 = BIO_new(BIO_f_base64());
    bio = BIO_push(b64, bio);

    BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL);

    BIO_read(bio, buffer, encoded_size);

    BIO_free_all(bio);

    return buffer;
}
```
Bagian fungsi utama
```c
int main(){
    // bagian untuk membuka file
    FILE *file = fopen("files/zip-pass.txt", "r");
    if(!file){
        perror("Error opening zip-pass.txt");
        return 1;
    }

    // membaca password dari file
    char password[MAX_PASSWORD_LENGTH];
    if(fgets(password, MAX_PASSWORD_LENGTH, file) == NULL){
        perror("Error reading password from zip-pass.txt");
        fclose(file);
        return 1;
    }

    size_t password_length = strlen(password);
    if(password_length > 0 && password[password_length - 1] == '\n'){
        password[password_length - 1] = '\0';
    }

    // menutup file
    fclose(file);

    // decode password
    char *decrypted_password = base64_decode(password);

    // membuat perintah untuk unzip
    char command[MAX_PASSWORD_LENGTH + 50];
    sprintf(command, "unzip -P %s files/home.zip -d homezip", decrypted_password);

    // melakukan command untuk unzip
    int result = system(command);
    if(result == 0){
        printf("File successfully extracted.\n");
    } else{
        printf("Error extracting file.\n");
    }

    // membersihkan memori
    free(decrypted_password);

    return 0;
}
```


### semangat.c
Bagian import library dan define versi fuse
```c
#define FUSE_USE_VERSION 28

#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/stat.h>
```
variabel global dirpath
```c
static const char *dirpath = "/home/renn/Praktikum/sisop/modul4/soal2/mounting";
```
Bagian fungsi untuk membuat folder
```c
void createFolder(const char *folder_path) {
    struct stat st = {0};

    if (stat(folder_path, &st) == -1) {
        mkdir(folder_path, 0700);
    }
}
```
Bagian fungsi untuk mengkategorikan file
```c
void categorizeFile(const char *file_name, const char *source_folder) {
    
    const char *dot = strrchr(file_name, '.');
    if (dot && dot != file_name) {
        
        const char *extension = dot + 1;
        
        const char *destination_folder;

        // bagian untuk mendefinisikan path file tergantung extension
        if (strcmp(extension, "pdf") == 0 || strcmp(extension, "docx") == 0) {
            destination_folder = "homezip/documents";
        } else if (strcmp(extension, "jpg") == 0 || strcmp(extension, "png") == 0 || strcmp(extension, "ico") == 0) {
            destination_folder = "homezip/images";
        } else if (strcmp(extension, "js") == 0 || strcmp(extension, "html") == 0 || strcmp(extension, "json") == 0) {
            destination_folder = "homezip/website";
        } else if (strcmp(extension, "c") == 0 || strcmp(extension, "sh") == 0) {
            destination_folder = "homezip/sisop";
        } else if (strcmp(extension, "txt") == 0) {
            destination_folder = "homezip/text";
        } else if (strcmp(extension, "ipynb") == 0 || strcmp(extension, "csv") == 0) {
            destination_folder = "homezip/aI";
        } else {
            return;
        }

        // membuat folder sesuai kategori
        createFolder(destination_folder);

        // deklarasi variable dan command
        char source_path[256];
        char destination_path[256];
        sprintf(source_path, "%s/%s", source_folder, file_name);
        sprintf(destination_path, "%s/%s", destination_folder, file_name);

        // error handling
        if (rename(source_path, destination_path) != 0) {
            perror("Error moving file");
        } else {
            printf("File %s moved to %s\n", file_name, destination_path);
        }
    }
}
```
Fungsi mkdir untuk membuat directory
```c
static int xmp_mkdir(const char *path, mode_t mode){
    int res;
    char fpath[1000];

    sprintf(fpath, "%s%s", dirpath, path);
    res = mkdir(fpath, mode);

    if(res == -1){
        return -errno;
    }

    return 0;
}
```
Fungsi copy file untuk mengcopy file 
```c
static int copy_file(const char *src, const char *dst){
    int res;
    char command[1000];

    sprintf(command, "cp -r %s %s", src, dst);

    res = system(command);

    if(res == -1){
        return -errno;
    }

    return 0;
}
```
Fungsi copy direktori untuk mengcopy direktori 
```c
static int copy_directory(const char *src, const char *dst){
    int res;
    char command[1000];

    sprintf(command, "cp -r %s/. %s", src, dst);

    res = system(command);

    if(res == -1){
        return -errno;
    }

    return 0;
}
```
Fungsi cp untuk copy
```c
static int xmp_cp(const char *src, const char *dst){
    struct stat st;
    int res;

    res = lstat(src, &st);
    if(res == -1){
        return -errno;
    }

    if(S_ISDIR(st.st_mode)){
        res = copy_directory(src, dst);
    } else{
        res = copy_file(src, dst);
    }

    return res;
}
```
Fungsi rm untuk menghapus file
```c
static int xmp_rm(const char *path){
    int res;
    char fpath[1000];

    sprintf(fpath, "%s%s", dirpath, path);

    if(strstr(fpath, "restricted") != NULL){
        printf("Cannot delete file with 'restricted' prefix.\n");
        return -EACCES; 
    }

    struct stat st;
    res = lstat(fpath, &st);
    if(res == -1){
        return -errno;
    }

    if(S_ISDIR(st.st_mode)){
        res = rmdir(fpath);
    } else{
        res = remove(fpath);
    }

    if(res == -1){
        return -errno;
    }

    return 0;
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];

    sprintf(fpath, "%s%s", dirpath, path);
    res = lstat(fpath, stbuf);

    if(res == -1) return -errno;
    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    DIR *dp;
    struct dirent *de;
    char fpath[1000];
    (void) offset;
    (void) fi;

    sprintf(fpath, "%s%s", dirpath, path);
    dp = opendir(fpath);

    if(dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL){
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        if(filler(buf, de->d_name, &st, 0)) break;
    }
    closedir(dp);
    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    int fd;
    int res;
    char fpath[1000];
    (void) fi;

    sprintf(fpath, "%s%s", dirpath, path);
    fd = open(fpath, O_RDONLY);

    if(fd == -1) return -errno;

    res = pread(fd, buf, size, offset);

    if(res == -1) res = -errno;

    close(fd);

    return res;
}

static struct fuse_operations xmp_oper ={
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .mkdir = xmp_mkdir,
    .cp = xmp_cp,
    .rm = xmp_rm,
};
```
Bagian funsi utama
```c
int main(int argc, char *argv[])
{   
``` 
Bagian untuk mengkategorikan file
```c
    // deklarasi variabel
    const char *folder_path = "homezip";

    // membuka direktori
    DIR *dir = opendir(folder_path);

    if (!dir) {
        perror("Error opening directory");
        return 1;
    }

    // membuaka file satu persatu dan mengkategorikannya
    printf("Categorizing file in [%s]", folder_path);
    sleep(1);
    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) {
            const char *file_name = entry->d_name;

            categorizeFile(file_name, folder_path);
        }
    }
    printf("Finishied categorizing file in [%s]", folder_path);
    sleep(1);

    // menutup file
    closedir(dir);
```
menjalankan fuse
```c    
    umask(0);
    return fuse_main(argc, argv, &xmp_oper, NULL);
}
```


### server.c
Bagian import library dan define 
```c
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

#define PORT 8080
#define MAX_LINE_LENGTH 100
#define MAX_ENTRIES 50
```
Bagian struct untuk webtoon
```c
struct Webtoon{
    char day[MAX_LINE_LENGTH];
    char genre[MAX_LINE_LENGTH];
    char title[MAX_LINE_LENGTH];
};
```
Bagian fungsi untuk menampilkan judul berdasarkan filter
```c
void displayTitles(struct Webtoon webtoons[], int numEntries, char filter[], char filterValue[]){
    printf("Titles based on %s %s:\n", filter, filterValue);

    // melakukan print file satu per satu tegantung filter
    int found = 0;
    for(int i = 0; i < numEntries; ++i){
        if((strcmp(filter, "hari") == 0 && strcmp(webtoons[i].day, filterValue) == 0) ||
            (strcmp(filter, "genre") == 0 && strcmp(webtoons[i].genre, filterValue) == 0) ||
            (strcmp(filter, "judul") == 0 && strcmp(webtoons[i].title, filterValue) == 0)){
            printf("%d. %s\n", found + 1, webtoons[i].title);
            found++;
        }
    }

    if(found == 0){
        printf("No titles found for %s %s.\n", filter, filterValue);
    }

    printf("\n");
}
```
Bagian fungsi untuk menampilkan semua judul
```c
void displayAllTitles(struct Webtoon webtoons[], int numEntries){
    printf("All Titles:\n");

    // print data di struct satu per satu
    for(int i = 0; i < numEntries; ++i){
        printf("%d. %s\n", i + 1, webtoons[i].title);
    }

    printf("\n");
}
```
Bagian fungsi untuk menambahkan novel
```c
void addWebtoon(struct Webtoon webtoons[], int *numEntries, char day[], char genre[], char title[]){
    // membaca file
    if(*numEntries < MAX_ENTRIES){
        // Menghapus spasi yang tidak diinginkan
        for(int i = 0; title[i]; i++){
            if(title[i] == ' ' && title[i + 1] == ','){
                for (int j = i; title[j]; j++){
                    title[j] = title[j + 1];
                }
                break;
            }
        }
        // menambah data ke struct
        strcpy(webtoons[*numEntries].day, day);
        strcpy(webtoons[*numEntries].genre, genre);
        strcpy(webtoons[*numEntries].title, title);
        (*numEntries)++;
    } 
    else{
        printf("Max entries reached. Cannot add more webtoons.\n");
    }
}
```
Bagian fungsi untuk save novel ke file
```c
void saveToFile(struct Webtoon webtoons[], int numEntries){
    // membuka file
    FILE *file = fopen("homezip/aI/webtoon.csv", "w");
    if(!file){
        perror("Error opening file");
        exit(EXIT_FAILURE);
    }

    // write file ke file
    for(int i = 0; i < numEntries; ++i){
        fprintf(file, "%s,%s,%s\n", webtoons[i].day, webtoons[i].genre, webtoons[i].title);
    }

    // menutup file
    fclose(file);
}
```
Bagian fungsi utama
```c
int main(int argc, char const *argv[]){
    // membuka file webtoon.csv
    FILE *file = fopen("homezip/aI/webtoon.csv", "r");
    if(!file){
        perror("Error opening file");
        return EXIT_FAILURE;
    }

    // membuat struct
    struct Webtoon webtoons[MAX_ENTRIES];
    int numEntries = 0;

    // melakukan parsing dan memasukan kedalam struct
    while (fscanf(file, "%[^,],%[^,],%[^\n]\n",
                  webtoons[numEntries].day,
                  webtoons[numEntries].genre,
                  webtoons[numEntries].title) == 3){
        numEntries++;
    }

    // menutup file
    fclose(file);

    // inisiasi variable untuk server
    int server_fd, new_socket;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);

    // membuat socket
    if((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0){
        perror("Socket creation failed");
        exit(EXIT_FAILURE);
    }

    if(setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))){
        perror("Setsockopt failed");
        exit(EXIT_FAILURE);
    }
    
    // set ip dan port
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    // socket binding dan listening
    if(bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0){
        perror("Bind failed");
        exit(EXIT_FAILURE);
    }

    if(listen(server_fd, 3) < 0){
        perror("Listen failed");
        exit(EXIT_FAILURE);
    }

    if((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen)) < 0){
        perror("Accept failed");
        exit(EXIT_FAILURE);
    }

    // loop message dengan client
    while (1){
        char message[100];
        int valread = read(new_socket, message, 1024);
        if(valread <= 0){
            break;
        }
        
        printf("Received: %s", message);

        fgets(message, sizeof(message), stdin);

        message[strcspn(message, "\n")] = '\0';

        char *command = strtok(message, " ");
        char *parameter1 = strtok(NULL, " ");
        char *parameter2 = strtok(NULL, "'");
        char *parameter3 = strtok(NULL, "'");

        char response[1024];
        if(strcmp(command, "exit") == 0){
            printf("Exiting the program.\n");
            break;
        } 
        else if(strcmp(command, "hari") == 0 || strcmp(command, "genre") == 0 || strcmp(command, "judul") == 0){
            displayTitles(webtoons, numEntries, command, parameter1);
        } 
        else if(strcmp(command, "semua") == 0){
            displayAllTitles(webtoons, numEntries);
        } 
        else if(strcmp(command, "tambah") == 0 && parameter1 != NULL && parameter2 != NULL && parameter3 != NULL){
            addWebtoon(webtoons, &numEntries, parameter1, parameter2, parameter3);
            saveToFile(webtoons, numEntries);
            printf("Webtoon added successfully.\n");
        } 
        else{
            printf("Invalid Command\n");
        }

        send(new_socket, response, strlen(response), 0);
    }

    close(new_socket);
    close(server_fd);

    return 0;
}
```

### output
- open-password.c
![](img/soal2img/gambar1.png)

## SOAL 3

**Penjelasan Soal**

Sarah harus membuat filesystem yang memiliki aturan khusus terkait direktori modular dengan awalan "module_". Saat membuat atau me-rename direktori dengan awalan tersebut, harus dilakukan modularisasi pada direktori dan sub-direktorinya. Setiap operasi dilogging dalam file "/home/[user]/fs_module.log" dengan level REPORT atau FLAG, tergantung pada jenis operasi. Pada level log FLAG, log akan mencatat setiap kali terjadi system call rmdir (untuk menghapus direktori) dan unlink (untuk menghapus file). Sedangkan untuk sisa operasi lainnya, akan dicatat dengan level log REPORT. Saat dilakukan modularisasi, file dipecah menjadi file-file kecil berukuran 1024 bytes. Jika sebuah direktori modular di-rename menjadi tidak modular, isi atau kontennya harus dikembalikan utuh.

**Penjelasan Code**

### Fungsi modular

```
// Fungsi rekursif untuk membagi file menjadi chunk-chunk kecil
void modular(char *path) {
    DIR *dir = opendir(path);
    struct dirent *entry;
    struct stat sb;

    while ((entry = readdir(dir))) {
        if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..")) continue;

        char curr[300];
        sprintf(curr, "%s/%s", path, entry->d_name);
        if (stat(curr, &sb) == -1) continue;

        if (S_ISDIR(sb.st_mode)) {
            modular(curr);
        } else if (S_ISREG(sb.st_mode)) {
            FILE *input = fopen(curr, "rb");
            if (input == NULL) return;

            char *buffer = (char *)malloc(sz);
            if (buffer == NULL) return;

            fseek(input, 0, SEEK_END);
            long fileSize = ftell(input);
            if (fileSize <= sz) continue;
            rewind(input);

            int count = (fileSize + sz - 1) / sz;

            for (int i = 0; i < count; i++) {
                char chunkNum[5], chunkPath[350];
                if (i < 10) sprintf(chunkNum, "00%d", i);
                else if (i < 100) sprintf(chunkNum, "0%d", i);
                else sprintf(chunkNum, "%d", i);
                sprintf(chunkPath, "%s.%s", curr, chunkNum);

                FILE *output = fopen(chunkPath, "wb");
                if (output == NULL) return;

                size_t bytes = fread(buffer, 1, sz, input);
                fwrite(buffer, 1, bytes, output);
                fclose(output);
            }
            fclose(input);

            char command[700];
            sprintf(command, "rm %s ", curr);
            system(command);

            free(buffer);
        }
    }
    closedir(dir);
}

```

**Keterangan:**

Code tersebut adalah implementasi fungsi rekursif yang bertujuan untuk membagi file menjadi chunk-chunk kecil. Berikut penjelasan mengenai kode tersebut:

1. `modular` adalah nama fungsi yang menerima parameter `path` yang merupakan path direktori yang ingin di-modularisasi.

2. `DIR *dir = opendir(path);` membuka direktori yang diberikan oleh `path` dan mengembalikan pointer ke struktur direktori (`DIR`).

3. `struct dirent *entry;` digunakan untuk menyimpan informasi setiap file/direktori dalam direktori yang sedang diolah.

4. `struct stat sb;` digunakan untuk menyimpan informasi status (seperti tipe file, ukuran, waktu modifikasi, dll.) dari setiap file/direktori.

5. Looping dengan `while ((entry = readdir(dir)))` untuk membaca setiap entri dalam direktori.

6. Pengecekan jika nama file/direktori adalah "." atau ".." maka akan diabaikan (`continue`), karena itu adalah entri untuk direktori saat ini dan direktori induk.

7. Pembentukan path lengkap dari file/direktori saat ini dengan `sprintf(curr, "%s/%s", path, entry->d_name);`.

8. `if (stat(curr, &sb) == -1) continue;` melakukan pengambilan informasi status dari file/direktori saat ini dan mengabaikannya jika gagal.

9. Pengecekan jenis file/direktori dengan `S_ISDIR` dan `S_ISREG`.
   - Jika jenisnya adalah direktori (`S_ISDIR`), maka fungsi rekursif (`modular`) dipanggil kembali untuk direktori tersebut.
   - Jika jenisnya adalah file regular (`S_ISREG`), maka file tersebut akan dipecah menjadi chunk-chunk kecil.

10. File dibuka dengan `fopen(curr, "rb")`, dan ukuran file dihitung dengan `fseek`, `ftell`, dan `rewind`.

11. Memori untuk buffer alokasi dinamis menggunakan `malloc(sz)`.

12. Perhitungan jumlah chunk yang akan dibuat.

13. Looping untuk membaca file, menulis setiap chunk ke file baru, dan menutup file.

14. File asli ditutup dan dihapus menggunakan `system("rm ...")`.

15. Membebaskan memori buffer yang telah dialokasikan.

### Fungsi Unmodular

```
// Fungsi rekursif untuk menggabungkan chunk-chunk menjadi satu file
void unmodular(const char *path){
    DIR *dir = opendir(path);
    struct dirent *entry;
    struct stat sb;
    while ((entry = readdir(dir))){
        if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..")) continue;
        char curr[300];
        snprintf(curr, sizeof(curr), "%s/%s", path, entry->d_name);
        if (stat(curr, &sb) == -1) continue;
        if (S_ISDIR(sb.st_mode)){
            unmodular(curr);
        }
        else if (S_ISREG(sb.st_mode) && strlen(curr) > 3 && !strcmp(curr + strlen(curr) - 3, "000")){
            int count = 0;
            char dest[300], temp[350];
            memset(dest, '\0', sizeof(dest));
            memset(temp, '\0', sizeof(temp));
            strncpy(dest, curr, strlen(curr) - 4);
            while(1){
                if (count < 10) snprintf(temp, sizeof(temp), "%s.00%d", dest, count);
                else if (count < 100) snprintf(temp, sizeof(temp), "%s.0%d", dest, count);
                else snprintf(temp, sizeof(temp), "%s.%d", dest, count);

                if (stat(temp, &sb)) break;
                FILE *destfile = fopen(dest, "ab");
                FILE *tempfile = fopen(temp, "rb");
                if (destfile && tempfile) {
                    char buffer[1024];
                    size_t read_bytes;
                    while ((read_bytes = fread(buffer, 1, sizeof(buffer), tempfile)) > 0) {
                        fwrite(buffer, 1, read_bytes, destfile);
                    }
                }
                if (tempfile) fclose(tempfile);
                if (destfile) fclose(destfile);
                remove(temp);
                count++;
            }
        }
    }
    closedir(dir);
}

```

**Keterangan:**

Code tersebut adalah implementasi fungsi rekursif untuk menggabungkan chunk-chunk kecil menjadi satu file. Berikut adalah penjelasan mengenai kode tersebut:

1. `void unmodular(const char *path)` adalah deklarasi fungsi yang menerima parameter `path`, yang merupakan path direktori yang berisi chunk-chunk kecil yang ingin digabungkan.

2. `DIR *dir = opendir(path);` membuka direktori yang diberikan oleh `path` dan mengembalikan pointer ke struktur direktori (`DIR`).

3. `struct dirent *entry;` digunakan untuk menyimpan informasi setiap file/direktori dalam direktori yang sedang diolah.

4. `struct stat sb;` digunakan untuk menyimpan informasi status (seperti tipe file, ukuran, waktu modifikasi, dll.) dari setiap file/direktori.

5. Looping dengan `while ((entry = readdir(dir)))` untuk membaca setiap entri dalam direktori.

6. Pengecekan jika nama file/direktori adalah "." atau ".." maka akan diabaikan (`continue`), karena itu adalah entri untuk direktori saat ini dan direktori induk.

7. Pembentukan path lengkap dari file/direktori saat ini dengan `snprintf(curr, sizeof(curr), "%s/%s", path, entry->d_name);`.

8. `if (stat(curr, &sb) == -1) continue;` melakukan pengambilan informasi status dari file/direktori saat ini dan mengabaikannya jika gagal.

9. Pengecekan jenis file/direktori dengan `S_ISDIR` dan `S_ISREG`.
    - Jika jenisnya adalah direktori (`S_ISDIR`), maka fungsi rekursif (`unmodular`) dipanggil kembali untuk direktori tersebut.
    - Jika jenisnya adalah file regular (`S_ISREG`) dan nama file berakhir dengan "000", maka chunk-chunk tersebut akan digabungkan.

10. Looping untuk menggabungkan chunk-chunk:
    - Menghitung path tujuan dan path sementara.
    - Membuka file tujuan dengan `fopen(dest, "ab")` dan file sementara dengan `fopen(temp, "rb")`.
    - Membaca data dari file sementara dan menuliskannya ke file tujuan.
    - Menutup kedua file.
    - Menghapus file sementara dengan `remove(temp)`.
    - Menaikkan counter untuk memproses chunk selanjutnya.

11. Direktori yang sedang diolah ditutup.

### Fungsi log
```
// Fungsi untuk menulis log
void writelog(const char *type, const char *call, const char *arg1, const char *arg2){
    time_t t = time(NULL);
    struct tm *curr = localtime(&t);
    char logmsg[1000];
    if (arg2 == NULL){
        snprintf(logmsg, sizeof(logmsg), "%s::%02d%02d%02d-%02d:%02d:%02d::%s::%s", type, (curr->tm_year + 1900) % 100, curr->tm_mon + 1, curr->tm_mday, curr->tm_hour, curr->tm_min, curr->tm_sec, call, arg1);
    }
    else {
        snprintf(logmsg, sizeof(logmsg), "%s::%02d%02d%02d-%02d:%02d:%02d::%s::%s::%s", type, (curr->tm_year + 1900) % 100, curr->tm_mon + 1, curr->tm_mday, curr->tm_hour, curr->tm_min, curr->tm_sec, call, arg1, arg2);
    }
    FILE *logfile = fopen(logpath, "a");
    if (logfile != NULL) {
        fprintf(logfile, "%s\n", logmsg);
        fclose(logfile);
    }
}

```

**Keterangan:**

Code tersebut adalah implementasi fungsi untuk menulis log. Berikut adalah penjelasan mengenai kode tersebut:

1. `void writelog(const char *type, const char *call, const char *arg1, const char *arg2)` adalah deklarasi fungsi yang menerima beberapa parameter:
   - `type`: Tipe log (misalnya, "ERROR", "FLAG", dll.).
   - `call`: Nama fungsi atau panggilan yang sedang dicatat.
   - `arg1` dan `arg2`: Argumen tambahan yang ingin dicatat (opsional).

2. `time_t t = time(NULL);` mendapatkan waktu saat ini dalam bentuk objek `time_t`.

3. `struct tm *curr = localtime(&t);` mengonversi waktu dalam objek `time_t` menjadi struktur `tm` yang berisi informasi waktu seperti tahun, bulan, hari, jam, menit, dan detik.

4. `char logmsg[1000];` menyediakan buffer untuk menyimpan pesan log.

5. Kondisional (`if (arg2 == NULL)`) digunakan untuk menentukan apakah ada dua argumen atau hanya satu.
   - Jika hanya satu argumen, maka pesan log dibuat tanpa `arg2`.
   - Jika ada dua argumen, pesan log akan mencakup kedua argumen tersebut.

6. `snprintf(logmsg, sizeof(logmsg), ...)` digunakan untuk memformat pesan log dengan informasi seperti tipe log, waktu, nama panggilan, dan argumen-argumen.

7. `FILE *logfile = fopen(logpath, "a");` membuka file log untuk ditulis dengan mode "a" (append), sehingga data baru akan ditambahkan ke akhir file.

8. Pengecekan apakah file log berhasil dibuka (`if (logfile != NULL)`).
   - Jika berhasil, pesan log ditulis ke file menggunakan `fprintf(logfile, "%s\n", logmsg)`.
   - File log ditutup dengan `fclose(logfile)`.

### Implementasi Fungsi Operasi Fuse

**Fungsi do_create**

```
// Fungsi untuk membuat file
static int do_create(const char *path, mode_t mode, struct fuse_file_info *fi){
	char fpath[1000];
	sprintf(fpath, "%s%s", dirpath, path);
	int res = open(fpath, fi->flags, mode);
	writelog("REPORT", "CREATE", (char*) path, (res == -1) ? strerror(errno) : NULL);
	return (res == -1) ? -errno : res;
}
```

**Keterangan:**
   - Fungsi ini digunakan untuk membuat file.
   - `path`: Path dari file yang ingin dibuat.
   - `mode`: Mode permission dari file yang baru dibuat.
   - `fi`: Pointer ke struktur `fuse_file_info` yang menyimpan informasi file.
   - Fungsi menggunakan `open()` untuk membuat file dan mengembalikan file descriptor.
   - Menulis log menggunakan `writelog()` dengan informasi operasi "CREATE".

**Fungsi do_release**

```
// Fungsi untuk melepaskan sumber daya setelah membuka file
static int do_release(const char *path, struct fuse_file_info *fi){
    writelog("REPORT", "RELEASE", (char*) path, NULL);
    return 0;
}
```

**Keterangan:**

   - Fungsi ini digunakan untuk melepaskan sumber daya setelah membuka file.
   - `path`: Path dari file yang dibuka.
   - `fi`: Pointer ke struktur `fuse_file_info`.
   - Fungsi ini hanya menulis log menggunakan `writelog()` dengan informasi operasi "RELEASE".

**Fungsi do_getattr**

```
// Fungsi untuk mendapatkan atribut file
static int do_getattr(const char *path, struct stat *stbuf){
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);
    int res = lstat(fpath, stbuf);
    writelog("REPORT", "GETATTR", (char*) path, (res == -1) ? strerror(errno) : NULL);
    return (res == -1) ? -errno : 0;
}
```

**Keterangan:**

   - Fungsi ini digunakan untuk mendapatkan atribut (metadata) dari file.
   - `path`: Path dari file yang atributnya ingin diperoleh.
   - `stbuf`: Pointer ke struktur `stat` untuk menyimpan informasi atribut.
   - Fungsi menggunakan `lstat()` untuk mendapatkan atribut dan menulis log menggunakan `writelog()` dengan informasi operasi "GETATTR".

**Fungsi do_readlink**

```
// Fungsi untuk membaca link
static  int  do_readlink(const char *path, char *buf, size_t size){
	char fpath[1000];
	sprintf(fpath, "%s%s", dirpath, path);
	int res = readlink(fpath, buf, size - 1);
	writelog("REPORT", "READLINK", (char*) path, (res == -1) ? strerror(errno) : NULL);
	return (res == -1) ? -errno : 0;
}

```

**Keterangan:**

   - Fungsi ini digunakan untuk membaca isi dari symlink.
   - `path`: Path dari symlink.
   - `buf`: Buffer untuk menyimpan isi symlink.
   - `size`: Ukuran buffer.
   - Fungsi menggunakan `readlink()` untuk membaca symlink dan menulis log menggunakan `writelog()` dengan informasi operasi "READLINK".

**Fungsi do_access**

```
// Fungsi untuk mengakses file
static int do_access(const char *path, int mode){
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);
    int res = access(fpath, mode);
    writelog("REPORT", "ACCESS", (char*) path, (res == -1) ? strerror(errno) : NULL);
    return (res == -1) ? -errno : 0;
}

```

**Keterangan:**

   - Fungsi ini digunakan untuk mengakses file atau direktori.
   - `path`: Path dari file atau direktori.
   - `mode`: Mode akses yang ingin diuji.
   - Fungsi menggunakan `access()` untuk menguji akses dan menulis log menggunakan `writelog()` dengan informasi operasi "ACCESS".

**Fungsi do_readdir**

```
// Fungsi untuk membaca direktori
static int do_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi){
	char fpath[1000];
	sprintf(fpath, "%s%s", dirpath, path);
	DIR *dp;
	struct dirent *de;
	(void) fi;
	(void) offset;
	dp = opendir(fpath);
	if (dp == NULL) return -errno;
	while ((de = readdir(dp))) {
    	struct stat st;
    	memset(&st, 0, sizeof(st));
    	st.st_ino = de->d_ino;
    	st.st_mode = de->d_type << 12;
    	if ((filler(buf, de->d_name, &st, 0)) != 0) break;
	}
	closedir(dp);
	writelog("REPORT", "READDIR", (char*) path, NULL);

	return 0;
}
```

**Keterangan:**

   - Fungsi ini digunakan untuk membaca isi dari direktori.
   - `path`: Path dari direktori.
   - `buf`: Buffer untuk menyimpan isi direktori.
   - `filler`: Fungsi untuk mengisi buffer dengan entri direktori.
   - `offset`: Offset untuk membaca direktori.
   - `fi`: Pointer ke struktur `fuse_file_info`.
   - Fungsi membuka direktori dengan `opendir()`, membaca setiap entri dengan `readdir()`, dan menulis log menggunakan `writelog()` dengan informasi operasi "READDIR".

**Fungsi do_mknod**

```
// Fungsi untuk membuat node 
static int do_mknod(const char *path, mode_t mode, dev_t dev){
	char fpath[1000];
	sprintf(fpath, "%s%s", dirpath, path);
	int res = mknod(fpath, mode, dev);
	writelog("REPORT", "MKNOD", (char*) path, (res == -1) ? strerror(errno) : NULL);
	return (res == -1) ? -errno : 0;
}

```

**Keterangan:**

   - Fungsi ini digunakan untuk membuat node (file, device file, atau symlink).
   - `path`: Path ke file atau node yang ingin dibuat.
   - `mode`: Mode permission dari file atau node yang baru dibuat.
   - `dev`: Device ID untuk node karakter atau node blok.
   - Fungsi menggunakan `mknod()` untuk membuat node dan menulis log menggunakan `writelog()` dengan informasi operasi "MKNOD".

**Fungsi do_link**

```
// Fungsi untuk membuat hard link
static int do_link(const char *from, const char *to){
	char frompath[1000], topath[1000];
	sprintf(frompath, "%s%s", dirpath, from);
	sprintf(topath, "%s%s", dirpath, to);
	int res = link(frompath, topath);
    if (res == -1) return -errno;
	writelog("REPORT", "LINK", (char*) from, (char*) to);

```

**Keterangan:**

   - Fungsi ini digunakan untuk membuat hard link dari suatu file.
   - `from`: Path ke file yang akan di-link.
   - `to`: Path baru untuk hard link.
   - Fungsi menggunakan `link()` untuk membuat hard link dan menulis log menggunakan `writelog()` dengan informasi operasi "LINK".

**Fungsi do_read**

```
// Fungsi untuk membaca data dari file
static int do_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi){
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int fd = open(fpath, O_RDONLY);
    if (fd == -1) return -errno;

    int res = pread(fd, buf, size, offset);
    if (res == -1) res = -errno;

    close(fd);
    writelog("REPORT", "READ", (char*) path, NULL);
    return res;
}

```

**Keterangan:**

   - Fungsi ini digunakan untuk membaca data dari suatu file.
   - `path`: Path ke file yang akan dibaca.
   - `buf`: Buffer untuk menyimpan data yang dibaca.
   - `size`: Ukuran data yang ingin dibaca.
   - `offset`: Offset pembacaan data.
   - `fi`: Pointer ke struktur `fuse_file_info`.
   - Fungsi membuka file dengan `open()`, membaca data dengan `pread()`, menutup file, dan menulis log menggunakan `writelog()` dengan informasi operasi "READ".

**Fungsi do_rename**

```
/ Fungsi untuk mengganti nama file atau memindahkan file
static int do_rename(const char *old, const char *new){
	char oldpath[1000], newpath[1000];
	sprintf(oldpath, "%s%s", dirpath, old);
	sprintf(newpath, "%s%s", dirpath, new);
	int res;
	res = rename(oldpath, newpath);
	if (res == -1) return -errno;
	writelog("REPORT", "RENAME", (char*) old, (char*) new);

	char curr[1000], dup[1000];
	strcpy(curr, dirpath);
	strcpy(dup, new);
	char *token, *oldname = strrchr(old, '/'), *newname = strrchr(new, '/');
	token = strtok(dup, "/");

	while(token != NULL){
    	strcat(curr, "/");
    	strcat(curr, token);
    	struct stat sb;
    	if (stat(curr, &sb) == -1) continue;
    	if (strlen(token) >= 7 && !strncmp(token, "module_", 7) && S_ISDIR(sb.st_mode)){
        	modular(curr);
        	break;
    	}
    	token = strtok(NULL, "/");
	}

	if (strcmp(oldname, newname)){
    	struct stat sb;
    	if ((stat(newpath, &sb) != -1) && S_ISDIR(sb.st_mode) && (strlen(newname) < 8 || strncmp(newname, "/module_", 8))) unmodular(newpath);
	}

	return 0;
}

```

**Keterangan:**

   - Fungsi ini digunakan untuk mengganti nama file atau memindahkan file.
   - `old`: Path ke file yang akan diubah namanya.
   - `new`: Path baru untuk file.
   - Fungsi menggunakan `rename()` untuk mengganti nama file, menulis log menggunakan `writelog()` dengan informasi operasi "RENAME", dan melakukan operasi modular atau unmodular berdasarkan pola nama file.

**Fungsi do_unlink**

```
// Fungsi untuk menghapus file
static int do_unlink(const char *path){
	char fpath[1000];
	sprintf(fpath, "%s%s", dirpath, path);
	int res = unlink(fpath);
    writelog("FLAG", "UNLINK", (char*) path, (res == -1) ? strerror(errno) : NULL);
	return (res == -1) ? -errno : 0;
}

```

**Keterangan:**

   - Fungsi ini digunakan untuk menghapus file.
   - `path`: Path ke file yang akan dihapus.
   - Fungsi menggunakan `unlink()` untuk menghapus file dan menulis log menggunakan `writelog()` dengan informasi operasi "UNLINK".

**Fungsi do_rmdir**

```
// Fungsi untuk menghapus direktori
static int do_rmdir(const char *path){
	char fpath[1000];
	sprintf(fpath, "%s%s", dirpath, path);
	int res = rmdir(fpath);
	writelog("FLAG", "RMDIR", (char*) path, (res == -1) ? strerror(errno) : NULL);
	return (res == -1) ? -errno : 0;
}

```

**Keterangan:**

   - Fungsi ini digunakan untuk menghapus direktori.
   - `path`: Path ke direktori yang akan dihapus.
   - Fungsi menggunakan `rmdir()` untuk menghapus direktori dan menulis log menggunakan `writelog()` dengan informasi operasi "RMDIR".

**Fungsi do_mkdir**

```
// Fungsi untuk membuat direktori
static int do_mkdir(const char *path, mode_t mode){
	char fpath[1000];
	sprintf(fpath, "%s%s", dirpath, path);
	int res = mkdir(fpath, mode);
	writelog("REPORT", "MKNOD", (char*) path, (res == -1) ? strerror(errno) : NULL);
	return (res == -1) ? -errno : 0;
}
```

**Keterangan:**

   - Fungsi ini digunakan untuk membuat direktori.
   - `path`: Path ke direktori yang akan dibuat.
   - `mode`: Mode permission dari direktori yang baru dibuat.
   - Fungsi menggunakan `mkdir()` untuk membuat direktori dan menulis log menggunakan `writelog()` dengan informasi operasi "MKNOD".

**Fungsi do_symlink**

```
// Fungsi untuk membuat link simbolik
static int do_symlink(const char *to, const char *from){
    char topath[1000], frompath[1000];
    sprintf(topath, "%s%s", dirpath, to);
    sprintf(frompath, "%s%s", dirpath, from);
    int res = symlink(topath, frompath);
    return (res == -1) ? -errno : (writelog("REPORT", "SYMLINK", (char*) to, (char*) from), 0);
}
```

**Keterangan:**

   - Fungsi ini digunakan untuk membuat link simbolik.
   - `to`: Path atau teks yang akan menjadi target dari symlink.
   - `from`: Path ke symlink yang akan dibuat.
   - Fungsi menggunakan `symlink()` untuk membuat symlink dan menulis log menggunakan `writelog()` dengan informasi operasi "SYMLINK".

**Fungsi do_truncate**

```
// Fungsi untuk mengganti ukuran file
static int do_truncate(const char *path, off_t size){
	char fpath[1000];
	sprintf(fpath, "%s%s", dirpath, path);
	int res = truncate(fpath, size);
	writelog("REPORT", "TRUNCATE", (char*) path, (res == -1) ? strerror(errno) : NULL);
	return (res == -1) ? -errno : 0;
}

```

**Keterangan:**

   - Fungsi ini digunakan untuk mengganti ukuran file.
   - `path`: Path ke file yang ukurannya akan diubah.
   - `size`: Ukuran baru dari file.
   - Fungsi menggunakan `truncate()` untuk mengganti ukuran file dan menulis log menggunakan `writelog()` dengan informasi operasi "TRUNCATE".

**Fungsi do_open**

```
// Fungsi untuk membuka file
static int do_open(const char *path, struct fuse_file_info *fi){
	char fpath[1000];
	sprintf(fpath, "%s%s", dirpath, path);
    int res = open(fpath, fi->flags);
    if (res == -1) return -errno;
    close(res);
	writelog("REPORT", "OPEN", (char*) path, NULL);
    return 0;
}
```

**Keterangan:**

   - Fungsi ini digunakan untuk membuka file.
   - `path`: Path ke file yang akan dibuka.
   - `fi`: Pointer ke struktur `fuse_file_info`.
   - Fungsi menggunakan `open()` untuk membuka file, menutup file, dan menulis log menggunakan `writelog()` dengan informasi operasi "OPEN".

**Fungsi do_chown**

```
// Fungsi untuk mengganti pemilik file
static int do_chown(const char *path, uid_t uid, gid_t gid){
	char fpath[1000];
	sprintf(fpath, "%s%s", dirpath, path);
	int res = chown(fpath, uid, gid);
	writelog("REPORT", "CHOWN", (char*) path, (res == -1) ? strerror(errno) : NULL);
	return (res == -1) ? -errno : 0;
}
```

**Keterangan:**

- Fungsi ini digunakan untuk mengganti pemilik fil.
- `path`: Path ke file yang pemiliknya akan diubah.
- `uid`: User ID baru.
- `gid`: Group ID baru.
- Fungsi menggunakan `chown()` untuk mengganti pemilik file dan menulis log menggunakan `writelog()` dengan informasi operasi "CHOWN".

**Fungsi do_write**

```
// Fungsi untuk menulis data ke file
static int do_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi){
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int fd = open(fpath, O_WRONLY);
    if (fd == -1) return -errno;

    int res = pwrite(fd, buf, size, offset);
    if (res == -1) res = -errno;

    close(fd);
    writelog("REPORT", "WRITE", (char*) path, NULL);
    return res;
}
```

**Keterangan:**

 - Fungsi ini digunakan untuk menulis data ke file.
 - `path`: Path ke file yang akan ditulis.
 - `buf`: Buffer yang berisi data yang akan ditulis.
 - `size`: Ukuran data yang akan ditulis.
 - `offset`: Offset penulisan data.
 - `fi`: Pointer ke struktur `fuse_file_info`.
 - Fungsi membuka file dengan `open()`, menulis data dengan `pwrite()`, menutup file, dan menulis log menggunakan `writelog()` dengan informasi operasi "WRITE".

**Fungsi do_fsync**

```
// Fungsi untuk memindahkan penunjuk baca/tulis dalam file
static int do_fsync(const char *path, int isdatasync, struct fuse_file_info *fi){
    writelog("REPORT", "FSYNC", (char*) path, NULL);
    return 0;
}

```

**Keterangan:**

- Fungsi ini digunakan untuk memastikan data ditulis ke disk.
- `path`: Path ke file yang akan disinkronkan.
- `isdatasync`: Flag yang menunjukkan apakah hanya data yang perlu disinkronkan.
- `fi`: Pointer ke struktur `fuse_file_info`.
- Fungsi hanya menulis log menggunakan `writelog()` dengan informasi operasi "FSYNC".

**Fungsi do_chmod**

```
// Fungsi untuk mengganti mode file
static int do_chmod(const char *path, mode_t mode){
	char fpath[1000];
	sprintf(fpath, "%s%s", dirpath, path);
	int res = chmod(fpath, mode);
	writelog("REPORT", "CHMOD", (char*) path, (res == -1) ? strerror(errno) : NULL);
	return (res == -1) ? -errno : 0;
}
```

**Keterangan:**

- Fungsi ini digunakan untuk mengganti mode file.
- `path`: Path ke file yang mode-nya akan diubah.
- `mode`: Mode permission baru.
- Fungsi menggunakan `chmod()` untuk mengganti mode file dan menulis log menggunakan `writelog()` dengan informasi operasi "CHMOD".

**Fungsi do_utimens**

```
// Fungsi untuk mengganti waktu akses dan modifikasi file
static int do_utimens(const char *path, const struct timespec ts[2]){
	char fpath[1000];
	sprintf(fpath, "%s%s", dirpath, path);
	struct timeval tv[2];
	tv[0].tv_sec = ts[0].tv_sec;
	tv[0].tv_usec = ts[0].tv_nsec / 1000;
    tv[1].tv_sec = ts[1].tv_sec;
    tv[1].tv_usec = ts[1].tv_nsec / 1000;
	int res = utimes(fpath, tv);
	writelog("REPORT", "UTIMENS", (char*) path, (res == -1) ? strerror(errno) : NULL);
	return (res == -1) ? -errno : 0;
}
```

**Keterangan:**

- Fungsi ini digunakan untuk mengganti waktu akses dan modifikasi file.
- `path`: Path ke file yang waktu akses dan modifikasinya akan diubah.
- `ts`: Array dari dua struktur `timespec` yang berisi waktu baru.
- Fungsi menggunakan `utimes()` untuk mengganti waktu file dan menulis log menggunakan `writelog()` dengan informasi operasi "UTIMENS".

**Fungsi do_getxattr**

```
// Fungsi untuk mendapatkan nilai atribut ekstended dari file
static int do_getxattr(const char *path, const char *name, char *value,size_t size){
	char fpath[1000];
	sprintf(fpath, "%s%s", dirpath, path);
    int res = lgetxattr(fpath, name, value, size);
    writelog("REPORT", "GETXATTR", (char*) path, (res == -1) ? strerror(errno) : NULL);
	return (res == -1) ? -errno : res;
}
```

**Keterangan:**

- Fungsi ini digunakan untuk mendapatkan nilai atribut ekstended dari file.
- `path`: Path ke file yang atributnya akan diperoleh.
- `name`: Nama atribut ekstended.
- `value`: Buffer untuk menyimpan nilai atribut.
- `size`: Ukuran buffer.
- Fungsi menggunakan `lgetxattr()` untuk mendapatkan nilai atribut dan menulis log menggunakan `writelog()` dengan informasi operasi "GETXATTR".

**Fungsi do_removexattr**

```
// Fungsi untuk menghapus atribut ekstended dari file
static int do_removexattr(const char *path, const char *name){
	char fpath[1000];
	sprintf(fpath, "%s%s", dirpath, path);
    int res = lremovexattr(fpath, name);
    writelog("REPORT", "REMOVEXATTR", (char*) path, (res == -1) ? strerror(errno) : NULL);
	return (res == -1) ? -errno : 0;
}
```

**Keterangan:**

- Fungsi ini digunakan untuk menghapus atribut ekstended dari file.
- `path`: Path ke file yang atributnya akan dihapus.
- `name`: Nama atribut ekstended yang akan dihapus.
- Fungsi menggunakan `lremovexattr()` untuk menghapus atribut ekstended dan menulis log menggunakan `writelog()` dengan informasi operasi "REMOVEXATTR".

**Fungsi do_listxattr**

```
// Fungsi untuk mendapatkan daftar atribut ekstended dari file
static int do_listxattr(const char *path, char *list, size_t size){
	char fpath[1000];
	sprintf(fpath, "%s%s", dirpath, path);
    int res = llistxattr(fpath, list, size);
    writelog("REPORT", "LISTXATTR", (char*) path, (res == -1) ? strerror(errno) : NULL);
	return (res == -1) ? -errno : res;
}
```

**Keterangan:**

- Fungsi ini digunakan untuk mendapatkan daftar atribut ekstended dari file.
- `path`: Path ke file yang daftar atributnya akan diperoleh.
- `list`: Buffer untuk menyimpan daftar atribut.
- `size`: Ukuran buffer.
- Fungsi menggunakan `llistxattr()` untuk mendapatkan daftar atribut ekstended dan menulis log menggunakan `writelog()` dengan informasi operasi "LISTXATTR".

**Fungsi do_setxattr**

```
// Fungsi untuk mengatur nilai atribut ekstended dari file
static int do_setxattr(const char *path, const char *name, const char *value, size_t size, int flags){
	char fpath[1000];
	sprintf(fpath, "%s%s", dirpath, path);
    int res = lsetxattr(fpath, name, value, size, flags);
    writelog("REPORT", "SETXATTR", (char*) path, (res == -1) ? strerror(errno) : NULL);
	return (res == -1) ? -errno : 0;
}
```

**Keterangan:**

- Fungsi ini digunakan untuk mengatur nilai atribut ekstended dari file.
- `path`: Path ke file yang atributnya akan diatur.
- `name`: Nama atribut ekstended.
- `value`: Nilai atribut ekstended.
- `size`: Ukuran nilai atribut.
- `flags`: Flags untuk mengatur atribut.
- Fungsi menggunakan `lsetxattr()` untuk mengatur nilai atribut ekstended dan menulis log menggunakan `writelog()` dengan informasi operasi "SETXATTR".

**Fungsi do_statfs**

```
// Fungsi untuk mendapatkan informasi tentang file system
static int do_statfs(const char *path, struct statvfs *stbuf){
	char fpath[1000];
	sprintf(fpath, "%s%s", dirpath, path);
    int res = statvfs(fpath, stbuf);
    writelog("REPORT", "STATFS", (char*) path, (res == -1) ? strerror(errno) : NULL);
	return (res == -1) ? -errno : 0;
}
```

**Keterangan:**

- Fungsi ini digunakan untuk mendapatkan informasi tentang file system.
- `path`: Path ke direktori yang terkait dengan file system.
- `stbuf`: Pointer ke struktur `statvfs` untuk menyimpan informasi file system.
- Fungsi menggunakan `statvfs()` untuk mendapatkan informasi file system dan menulis log menggunakan `writelog()` dengan informasi operasi "STATFS".

**Fungsi Main**
```
int main(int  argc, char *argv[]){
	umask(0);
	return fuse_main(argc, argv, &do_oper, NULL);
}
```

**Keterangan:**

1. **`umask(0);`**
   - `umask` digunakan untuk mengatur umask program, yaitu hak akses default yang akan diabaikan saat membuat file atau direktori.
   - Dengan `umask(0);`, umask diatur menjadi 0, yang berarti tidak ada hak akses yang diabaikan. Ini berarti file atau direktori yang dibuat akan memiliki hak akses yang sesuai dengan nilai mode yang ditentukan pada saat pembuatan.

2. **`return fuse_main(argc, argv, &do_oper, NULL);`**
   - Fungsi ini memanggil `fuse_main` untuk memulai FUSE.
   - `argc` dan `argv` adalah argumen dari baris perintah yang diberikan saat program dijalankan.
   - `&do_oper` adalah pointer ke struktur yang berisi implementasi fungsi-fungsi operasi FUSE, seperti yang telah Anda definisikan sebelumnya (seperti `do_create`, `do_release`, dsb.).
   - `NULL` adalah pointer ke data yang dapat disimpan dan diakses di dalam implementasi operasi FUSE, namun dalam kasus ini tidak digunakan (diberikan nilai `NULL`).

### Langkah Pengerjaan

**1. Menjalankan Program Fuse**

![Alt Text](img/soal3img/Screenshot_from_2023-11-18_17-52-52.png)

**Output:**

![Alt Text](img/soal3img/Screenshot_from_2023-11-18_17-54-33.png)

**2. _Rename_ Direktori**

![Alt Text](img/soal3img/Screenshot_from_2023-11-18_18-48-15.png)

**Output:**

![Alt Text](img/soal3img/Screenshot_from_2023-11-18_18-48-47.png)

**3. _Rename_ Direktori Dengan Nama Awal**

![Alt Text](img/soal3img/unmodular1.png)

**4. TestCase**

- cat file

![Alt Text](img/soal3img/Screenshot_from_2023-11-18_18-49-32.png)

- hapus (unlink) file

![Alt Text](img/soal3img/Screenshot_from_2023-11-18_18-50-24.png)

- copy file

![Alt Text](img/soal3img/Screenshot_from_2023-11-18_19-07-13.png)

**5. Log**

![Alt Text](img/soal3img/Screenshot_from_2023-11-18_19-02-34.png)

**Output:**

- unlik (Hapus file)

![Alt Text](img/soal3img/Screenshot_from_2023-11-18_19-15-13.png)

- rmdir (Hapus Direktori)

![Alt Text](img/soal3img/Screenshot_from_2023-11-18_19-16-16.png)


#### Kendala
tidak ada

#### Revisi
tidak ada





