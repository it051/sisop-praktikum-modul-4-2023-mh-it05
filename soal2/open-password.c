#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <openssl/buffer.h>

#define MAX_PASSWORD_LENGTH 256

// gcc open-password.c -o open-password -lssl -lcrypto

char *base64_decode(const char *encoded){
    BIO *bio, *b64;
    int encoded_size = strlen(encoded);

    char *buffer = (char *)malloc(encoded_size);
    memset(buffer, 0, encoded_size);

    bio = BIO_new_mem_buf(encoded, -1);
    b64 = BIO_new(BIO_f_base64());
    bio = BIO_push(b64, bio);

    BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL);

    BIO_read(bio, buffer, encoded_size);

    BIO_free_all(bio);

    return buffer;
}

int main(){
    FILE *file = fopen("files/zip-pass.txt", "r");
    if(!file){
        perror("Error opening zip-pass.txt");
        return 1;
    }

    char password[MAX_PASSWORD_LENGTH];
    if(fgets(password, MAX_PASSWORD_LENGTH, file) == NULL){
        perror("Error reading password from zip-pass.txt");
        fclose(file);
        return 1;
    }

    size_t password_length = strlen(password);
    if(password_length > 0 && password[password_length - 1] == '\n'){
        password[password_length - 1] = '\0';
    }

    fclose(file);

    char *decrypted_password = base64_decode(password);

    char command[MAX_PASSWORD_LENGTH + 50];
    sprintf(command, "unzip -P %s files/home.zip -d homezip", decrypted_password);

    int result = system(command);
    if(result == 0){
        printf("File successfully extracted.\n");
    } else{
        printf("Error extracting file.\n");
    }

    free(decrypted_password);

    return 0;
}
