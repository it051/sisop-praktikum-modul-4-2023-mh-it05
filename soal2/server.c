#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

#define PORT 8080
#define MAX_LINE_LENGTH 100
#define MAX_ENTRIES 50

struct Webtoon{
    char day[MAX_LINE_LENGTH];
    char genre[MAX_LINE_LENGTH];
    char title[MAX_LINE_LENGTH];
};

void displayTitles(struct Webtoon webtoons[], int numEntries, char filter[], char filterValue[]){
    printf("Titles based on %s %s:\n", filter, filterValue);

    int found = 0;
    for(int i = 0; i < numEntries; ++i){
        if((strcmp(filter, "hari") == 0 && strcmp(webtoons[i].day, filterValue) == 0) ||
            (strcmp(filter, "genre") == 0 && strcmp(webtoons[i].genre, filterValue) == 0) ||
            (strcmp(filter, "judul") == 0 && strcmp(webtoons[i].title, filterValue) == 0)){
            printf("%d. %s\n", found + 1, webtoons[i].title);
            found++;
        }
    }

    if(found == 0){
        printf("No titles found for %s %s.\n", filter, filterValue);
    }

    printf("\n");
}

void displayAllTitles(struct Webtoon webtoons[], int numEntries){
    printf("All Titles:\n");

    for(int i = 0; i < numEntries; ++i){
        printf("%d. %s\n", i + 1, webtoons[i].title);
    }

    printf("\n");
}

void addWebtoon(struct Webtoon webtoons[], int *numEntries, char day[], char genre[], char title[]){
    if(*numEntries < MAX_ENTRIES){
        // Menghapus spasi yang tidak diinginkan
        for(int i = 0; title[i]; i++){
            if(title[i] == ' ' && title[i + 1] == ','){
                for (int j = i; title[j]; j++){
                    title[j] = title[j + 1];
                }
                break;
            }
        }

        strcpy(webtoons[*numEntries].day, day);
        strcpy(webtoons[*numEntries].genre, genre);
        strcpy(webtoons[*numEntries].title, title);
        (*numEntries)++;
    } 
    else{
        printf("Max entries reached. Cannot add more webtoons.\n");
    }
}

void saveToFile(struct Webtoon webtoons[], int numEntries){
    FILE *file = fopen("homezip/aI/webtoon.csv", "w");
    if(!file){
        perror("Error opening file");
        exit(EXIT_FAILURE);
    }

    for(int i = 0; i < numEntries; ++i){
        fprintf(file, "%s,%s,%s\n", webtoons[i].day, webtoons[i].genre, webtoons[i].title);
    }

    fclose(file);
}


int main(int argc, char const *argv[]){
    FILE *file = fopen("homezip/aI/webtoon.csv", "r");
    if(!file){
        perror("Error opening file");
        return EXIT_FAILURE;
    }

    struct Webtoon webtoons[MAX_ENTRIES];
    int numEntries = 0;

    while (fscanf(file, "%[^,],%[^,],%[^\n]\n",
                  webtoons[numEntries].day,
                  webtoons[numEntries].genre,
                  webtoons[numEntries].title) == 3){
        numEntries++;
    }

    fclose(file);

    int server_fd, new_socket;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);

    if((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0){
        perror("Socket creation failed");
        exit(EXIT_FAILURE);
    }

    if(setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))){
        perror("Setsockopt failed");
        exit(EXIT_FAILURE);
    }
    
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    if(bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0){
        perror("Bind failed");
        exit(EXIT_FAILURE);
    }

    if(listen(server_fd, 3) < 0){
        perror("Listen failed");
        exit(EXIT_FAILURE);
    }

    if((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen)) < 0){
        perror("Accept failed");
        exit(EXIT_FAILURE);
    }

    while (1){
        char message[100];
        int valread = read(new_socket, message, 1024);
        if(valread <= 0){
            break;
        }
        
        printf("Received: %s", message);

        fgets(message, sizeof(message), stdin);

        message[strcspn(message, "\n")] = '\0';

        char *command = strtok(message, " ");
        char *parameter1 = strtok(NULL, " ");
        char *parameter2 = strtok(NULL, "'");
        char *parameter3 = strtok(NULL, "'");

        char response[1024];
        if(strcmp(command, "exit") == 0){
            printf("Exiting the program.\n");
            break;
        } 
        else if(strcmp(command, "hari") == 0 || strcmp(command, "genre") == 0 || strcmp(command, "judul") == 0){
            displayTitles(webtoons, numEntries, command, parameter1);
        } 
        else if(strcmp(command, "semua") == 0){
            displayAllTitles(webtoons, numEntries);
        } 
        else if(strcmp(command, "tambah") == 0 && parameter1 != NULL && parameter2 != NULL && parameter3 != NULL){
            addWebtoon(webtoons, &numEntries, parameter1, parameter2, parameter3);
            saveToFile(webtoons, numEntries);
            printf("Webtoon added successfully.\n");
        } 
        else{
            printf("Invalid Command\n");
        }

        send(new_socket, response, strlen(response), 0);
    }

    close(new_socket);
    close(server_fd);

    return 0;
}
