#define FUSE_USE_VERSION 28

#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/stat.h>

static const char *dirpath = "/home/renn/Praktikum/sisop/modul4/soal2/mounting";

static int xmp_mkdir(const char *path, mode_t mode){
    int res;
    char fpath[1000];

    sprintf(fpath, "%s%s", dirpath, path);
    res = mkdir(fpath, mode);

    if(res == -1){
        return -errno;
    }

    return 0;
}

static int copy_file(const char *src, const char *dst){
    int res;
    char command[1000];

    sprintf(command, "cp -r %s %s", src, dst);

    res = system(command);

    if(res == -1){
        return -errno;
    }

    return 0;
}

static int copy_directory(const char *src, const char *dst){
    int res;
    char command[1000];

    sprintf(command, "cp -r %s/. %s", src, dst);

    res = system(command);

    if(res == -1){
        return -errno;
    }

    return 0;
}

static int xmp_cp(const char *src, const char *dst){
    struct stat st;
    int res;

    res = lstat(src, &st);
    if(res == -1){
        return -errno;
    }

    if(S_ISDIR(st.st_mode)){
        res = copy_directory(src, dst);
    } else{
        res = copy_file(src, dst);
    }

    return res;
}

static int xmp_rm(const char *path){
    int res;
    char fpath[1000];

    sprintf(fpath, "%s%s", dirpath, path);

    if(strstr(fpath, "restricted") != NULL){
        printf("Cannot delete file with 'restricted' prefix.\n");
        return -EACCES; 
    }

    struct stat st;
    res = lstat(fpath, &st);
    if(res == -1){
        return -errno;
    }

    if(S_ISDIR(st.st_mode)){
        res = rmdir(fpath);
    } else{
        res = remove(fpath);
    }

    if(res == -1){
        return -errno;
    }

    return 0;
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];

    sprintf(fpath, "%s%s", dirpath, path);
    res = lstat(fpath, stbuf);

    if(res == -1) return -errno;
    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    DIR *dp;
    struct dirent *de;
    char fpath[1000];
    (void) offset;
    (void) fi;

    sprintf(fpath, "%s%s", dirpath, path);
    dp = opendir(fpath);

    if(dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL){
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        if(filler(buf, de->d_name, &st, 0)) break;
    }
    closedir(dp);
    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    int fd;
    int res;
    char fpath[1000];
    (void) fi;

    sprintf(fpath, "%s%s", dirpath, path);
    fd = open(fpath, O_RDONLY);

    if(fd == -1) return -errno;

    res = pread(fd, buf, size, offset);

    if(res == -1) res = -errno;

    close(fd);

    return res;
}

static struct fuse_operations xmp_oper ={
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .mkdir = xmp_mkdir,
    .cp = xmp_cp,
    .rm = xmp_rm,
};

int main(int argc, char *argv[])
{
    umask(0);
    return fuse_main(argc, argv, &xmp_oper, NULL);
}
