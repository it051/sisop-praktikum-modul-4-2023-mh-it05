#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>   

#define GALLERY_PATH "/home/p/modul4/soal1/data/gallery/"
#define REV_FOLDER "rev"
#define DELETE_FOLDER "delete"

void processRevFolder(const char *folderPath);
void processDeleteFolder(const char *folderPath);
void reverseFileName(char *fileName);
void createFolderIfNotExists(const char *folderPath);

int main() {
     
    createFolderIfNotExists(GALLERY_PATH REV_FOLDER);
    createFolderIfNotExists(GALLERY_PATH DELETE_FOLDER);

    while (1) {
        processRevFolder(GALLERY_PATH REV_FOLDER);
        processDeleteFolder(GALLERY_PATH DELETE_FOLDER);
        sleep(1);  
    }

    return 0;
}

void createFolderIfNotExists(const char *folderPath) {
    struct stat st = {0};

    if (stat(folderPath, &st) == -1) {
         
        if (mkdir(folderPath, 0777) == -1) {
            perror("Unable to create folder");
            exit(EXIT_FAILURE);
        }
    }
}

void processRevFolder(const char *folderPath) {
    DIR *dir;
    struct dirent *entry;

    dir = opendir(folderPath);

    if (dir == NULL) {
        perror("Unable to open directory");
        exit(EXIT_FAILURE);
    }

    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) {  
            char oldPath[512];   
            char newPath[512];  

            snprintf(oldPath, sizeof(oldPath), "%s/%s", folderPath, entry->d_name);
            snprintf(newPath, sizeof(newPath), "%s/%s", GALLERY_PATH, entry->d_name);

            
            reverseFileName(newPath);

            
            rename(oldPath, newPath);
        }
    }

    closedir(dir);
}

void processDeleteFolder(const char *folderPath) {
    DIR *dir;
    struct dirent *entry;

    dir = opendir(folderPath);

    if (dir == NULL) {
        perror("Unable to open directory");
        exit(EXIT_FAILURE);
    }

    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) { 
            char filePath[512];  
            snprintf(filePath, sizeof(filePath), "%s/%s", folderPath, entry->d_name);

            
            remove(filePath);
        }
    }

    closedir(dir);
}

void reverseFileName(char *fileName) {
    int i, j;
    char temp;

    for (i = 0, j = strlen(fileName) - 1; i < j; i++, j--) {
        temp = fileName[i];
        fileName[i] = fileName[j];
        fileName[j] = temp;
    }
}
