#define FUSE_USE_VERSION 28
#include <dirent.h>
#include <sys/time.h>
#include <sys/xattr.h>
#include <stdbool.h>
#include <unistd.h>
#include <fuse.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>

// Maksimum ukuran input dan ukuran blok
#define MAX_INPUT_SIZE 1024
#define MAX_SIZE 1024

// Path untuk file log dan direktori utama
static const char *logpath = "/home/monic/fs_module.log";
static const char *dirpath = "/home/monic";

// Deklarasi fungsi-fungsi
void modular(const char *path);
void unmodular(const char *path);
void create_log(const char *type, const char *call, const char *arg1, const char *arg2, const char *logpath);
static int do_getattr(const char *path, struct stat *stbuf);

// Fungsi helper untuk mengecek apakah suatu direktori memiliki awalan "module_"
int is_modular_directory(const char *path) {
    const char *prefix = "/module_";
    return strncmp(path, prefix, strlen(prefix)) == 0;
}

// Fungsi untuk modularisasi file
void modular(const char *path) {
    FILE *originalFile = fopen(path, "rb");
    if (originalFile == NULL) {
        perror("Error opening original file");
        create_log("ERROR", "MODULAR", path, "Gagal membuka file asli", logpath);
        return;
    }

    char buffer[MAX_SIZE];
    size_t bytesRead;

    int count = 0;
    do {
        bytesRead = fread(buffer, 1, MAX_SIZE, originalFile);

        char partPath[1000];
        snprintf(partPath, sizeof(partPath), "%s.%03d", path, count);

        FILE *partFile = fopen(partPath, "wb");
        if (partFile == NULL) {
            perror("Error creating part file");
            create_log("ERROR", "MODULAR", path, "Gagal membuat file bagian", logpath);
            fclose(originalFile);
            return;
        }

        size_t bytesToWrite = (bytesRead < MAX_SIZE) ? bytesRead : MAX_SIZE;
        size_t bytesWritten = fwrite(buffer, 1, bytesToWrite, partFile);
        if (bytesWritten != bytesToWrite) {
            perror("Error writing to part file");
            create_log("ERROR", "MODULAR", path, "Gagal menulis ke file bagian", logpath);
            fclose(partFile);
            fclose(originalFile);
            return;
        }

        fclose(partFile);
        create_log("REPORT", "MODULAR", partPath, "File bagian berhasil dibuat", logpath);
        count++;
    } while (bytesRead == MAX_SIZE);

    fclose(originalFile);
    create_log("REPORT", "MODULAR", path, "File berhasil dimodularisasi", logpath);
}

// Fungsi untuk mengembalikan file dari moduler ke utuh
void unmodular(const char *path) {
    FILE *outputFile = fopen(path, "wb");
    if (outputFile == NULL) {
        perror("Error opening output file");
        create_log("ERROR", "UNMODULAR", path, "Gagal membuka file keluaran", logpath);
        return;
    }

    char partPath[1000];
    int i = 0;

    while (1) {
        snprintf(partPath, sizeof(partPath), "%s.%03d", path, i);

        FILE *inputFile = fopen(partPath, "rb");
        if (inputFile == NULL) {
            break;
        }

        char buffer[MAX_SIZE];
        size_t bytesRead;
        while ((bytesRead = fread(buffer, 1, sizeof(buffer), inputFile)) > 0) {
            fwrite(buffer, 1, bytesRead, outputFile);
        }

        fclose(inputFile);
        remove(partPath);
        i++;
    }

    fclose(outputFile);
    create_log("REPORT", "UNMODULAR", path, "File berhasil dikembalikan dari moduler", logpath);
}

// Fungsi untuk membuat log
void create_log(const char *type, const char *call, const char *arg1, const char *arg2, const char *logpath) {
    time_t t = time(NULL);
    struct tm *curr = localtime(&t);
    char logmsg[1000];

    if (arg2 == NULL) {
        snprintf(logmsg, sizeof(logmsg), "%s::%02d%02d%02d-%02d:%02d:%02d::%s::%s", type, (curr->tm_year + 1900) % 100, curr->tm_mon + 1, curr->tm_mday, curr->tm_hour, curr->tm_min, curr->tm_sec, call, arg1);
    } else {
        snprintf(logmsg, sizeof(logmsg), "%s::%02d%02d%02d-%02d:%02d:%02d::%s::%s::%s", type, (curr->tm_year + 1900) % 100, curr->tm_mon + 1, curr->tm_mday, curr->tm_hour, curr->tm_min, curr->tm_sec, call, arg1, arg2);
    }

    FILE *logfile = fopen(logpath, "a");
    if (logfile != NULL) {
        fprintf(logfile, "%s\n", logmsg);
        fclose(logfile);
    } else {
        perror("Error opening log file");
    }
}

// Implementasi system call CREATE
static int do_create(const char *path, mode_t mode, struct fuse_file_info *fi) {
    char fpath[1000];
    snprintf(fpath, sizeof(fpath), "%s%s", dirpath, path);
    int res = open(fpath, fi->flags, mode);
    create_log("REPORT", "CREATE", (char *)path, (res == -1) ? strerror(errno) : NULL, logpath);
    return (res == -1) ? -errno : res;
}

// Implementasi system call GETATTR
static int do_getattr(const char *path, struct stat *stbuf) {
    int res;
    char fpath[1000];
    snprintf(fpath, sizeof(fpath), "%s%s", dirpath, path);
    res = lstat(fpath, stbuf);
    if (res == -1) {
        return -errno;
    }
    return 0;
}

// Implementasi system call READ
static int do_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
    int fd;
    int res;
    char fpath[1000];
    snprintf(fpath, sizeof(fpath), "%s%s", dirpath, path);
    fd = open(fpath, O_RDONLY);
    if (fd == -1) {
        return -errno;
    }
    res = pread(fd, buf, size, offset);
    if (res == -1) {
        res = -errno;
    }
    close(fd);
    return res;
}

// Implementasi system call WRITE
static int do_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
    int fd;
    int res;
    char fpath[1000];
    snprintf(fpath, sizeof(fpath), "%s%s", dirpath, path);
    fd = open(fpath, O_WRONLY);
    if (fd == -1) {
        return -errno;
    }
    res = pwrite(fd, buf, size, offset);
    if (res == -1) {
        res = -errno;
    }
    close(fd);
    return res;
}

// Implementasi system call OPEN
static int do_open(const char *path, struct fuse_file_info *fi) {
    int fd;
    char fpath[1000];
    snprintf(fpath, sizeof(fpath), "%s%s", dirpath, path);
    fd = open(fpath, fi->flags);
    if (fd == -1) {
        return -errno;
    }
    fi->fh = fd;
    return 0;
}

// Implementasi system call READDIR
static int do_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;
    char fpath[1000];
    snprintf(fpath, sizeof(fpath), "%s%s", dirpath, path);
    dp = opendir(fpath);
    if (dp == NULL) {
        return -errno;
    }
    while ((de = readdir(dp)) != NULL) {
        struct stat st;
        memset(&st, 0, sizeof(st));
        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        if (filler(buf, de->d_name, &st, 0)) {
            break;
        }
    }
    closedir(dp);
    return 0;
}

// Implementasi system call MKDIR
static int do_mkdir(const char *path, mode_t mode) {
    int res;
    char fpath[1000];
    snprintf(fpath, sizeof(fpath), "%s%s", dirpath, path);
    res = mkdir(fpath, mode);
    if (res == -1) {
        return -errno;
    }

    // Cek apakah direktori yang dibuat adalah modular
    if (is_modular_directory(fpath)) {
        modular(fpath);
    }

    return 0;
}

// Implementasi system call RMDIR
static int do_rmdir(const char *path) {
    int res;
    char fpath[1000];
    snprintf(fpath, sizeof(fpath), "%s%s", dirpath, path);
    res = rmdir(fpath);
    if (res == -1) {
        return -errno;
    }

    // Logging untuk operasi rmdir
    create_log("FLAG", "RMDIR", fpath, NULL, logpath);

    return 0;
}

// Implementasi system call RENAME
static int do_rename(const char *from, const char *to) {
    int res;
    char fpath_from[1000];
    char fpath_to[1000];
    snprintf(fpath_from, sizeof(fpath_from), "%s%s", dirpath, from);
    snprintf(fpath_to, sizeof(fpath_to), "%s%s", dirpath, to);
    res = rename(fpath_from, fpath_to);
    if (res == -1) {
        return -errno;
    }

    // Cek apakah direktori yang di-rename adalah modular
    if (is_modular_directory(fpath_to)) {
        unmodular(fpath_to);
    }

    // Logging untuk operasi rename
    create_log("REPORT", "RENAME", fpath_from, fpath_to, logpath);

    return 0;
}

// Implementasi system call UNLINK
static int do_unlink(const char *path) {
    int res;
    char fpath[1000];
    snprintf(fpath, sizeof(fpath), "%s%s", dirpath, path);
    res = unlink(fpath);
    if (res == -1) {
        return -errno;
    }

    // Logging untuk operasi unlink
    create_log("FLAG", "UNLINK", fpath, NULL, logpath);

    return 0;
}

// Struktur untuk operasi-operasi FUSE
static struct fuse_operations fuse_oper = {
    .getattr = do_getattr,
    .readdir = do_readdir,
    .read = do_read,
    .write = do_write,
    .open = do_open,
    .create = do_create,
    .mkdir = do_mkdir,
    .rmdir = do_rmdir,
    .rename = do_rename,
    .unlink = do_unlink,
};

// Fungsi utama
int main(int argc, char *argv[]) {
    return fuse_main(argc, argv, &fuse_oper, NULL);
}

